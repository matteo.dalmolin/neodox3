# The Neodox3 keyboard

**Neodox3** project.(based on skeletyl of bastard)

- Keyboard Maintainer: [MatteoDM](todo)  
- Hardware Supported: rp2040
- Hardware Availability: bastard keyboards

Make example for this keyboard (after setting up your build environment):

    make neodox2:neodimio

Enter the bootloader when prompted by doing one of the following:

- **Physical boot button**: Unplug your keyboard then press the BOOT button on the microcontroller PCB

- **Keycode in layout**: Press the key mapped to `QK_BOOT` if you have a layout that has one.

Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

- [Keymap](#keymap)

## Keymap

![Neodimio keymap](./keymaps/neodimio/imgs/neodimio.svg)

## Buildserver_docker_image

How to build
docker build -t 190212/qmk_firmware:latest -t 190212/qmk_firmware:0.27.1  .

How to push docker image
docker push 190212/qmk_firmware:0.27.1
